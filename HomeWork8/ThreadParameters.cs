﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace HomeWork8
{
    internal class ThreadParameters
    {
        public int StartingIndex { get; set; }
        public int EndIndex { get; set; }
        public int Sum { get; set; }
        public int[] TestArray { get; set; }
        public AutoResetEvent WaitHandler { get; set; }
        public ThreadParameters(int[] testArray, int startingIndex, int endIndex)
        {
            TestArray = testArray;
            StartingIndex = startingIndex;
            EndIndex = endIndex;
            WaitHandler = new AutoResetEvent(true);
        }
    }
}

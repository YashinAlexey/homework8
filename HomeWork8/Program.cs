﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace HomeWork8
{
    class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();
            Stopwatch stopWatch = new Stopwatch();
            int[] testArray = new int[10000000];
            int sum = 0;

            for (int i = 0; i < testArray.Length; i++)
                testArray[i] = random.Next(-2, 2);            
            
            stopWatch.Start();
            for (int i = 0; i < testArray.Length; i++)
                sum += testArray[i];
            stopWatch.Stop();

            Console.WriteLine($"Обычное суммирование : {sum} {stopWatch.ElapsedMilliseconds} мс");
            
            stopWatch.Restart();
            sum = CalculatingAmountUsingStreams(testArray, 5);
            stopWatch.Stop();

            Console.WriteLine($"Параллельное суммирование : {sum} {stopWatch.ElapsedMilliseconds} мс");

            stopWatch.Restart();
            sum = testArray.AsParallel().Sum(x => x);
            stopWatch.Stop();

            Console.WriteLine($"Параллельное суммирование с помощью LINQ: {sum} {stopWatch.ElapsedMilliseconds} мс");

            Console.ReadLine();
        }

        private static int CalculatingAmountUsingStreams(int[] testArray, int threadCount)
        {
            ThreadParameters[] threadParameters = new ThreadParameters[Math.Max(1, threadCount)];
            int step = 0, currentIndex = 0;

            if (threadCount == 0)            
                step = testArray.Length;            
            else            
                step = testArray.Length / threadCount;            

            int index = 0;
            while (currentIndex < testArray.Length)
            {
                ThreadParameters parameters = new ThreadParameters(testArray: testArray,
                                                                   startingIndex : currentIndex,
                                                                   endIndex : Math.Min(currentIndex + step, testArray.Length - 1));
                ThreadPool.QueueUserWorkItem(CalculatingAmount, parameters);
                threadParameters[index++] = parameters;
                currentIndex += ++step;
            }

            AutoResetEvent.WaitAll(threadParameters.Select(tp => tp.WaitHandler).ToArray());

            return threadParameters.Sum(tp => tp.Sum);
        }

        private static void CalculatingAmount(object inputParameters)
        {
            ThreadParameters parameters = (ThreadParameters)inputParameters;
            parameters.WaitHandler.WaitOne();

            for (int i = parameters.StartingIndex; i <= parameters.EndIndex; i++)
                parameters.Sum += parameters.TestArray[i];

            parameters.WaitHandler.Set();
        }
    }
}
